package com.ardab.subsapp.mobilepackage.controller;

import com.ardab.subsapp.mobilepackage.model.MobilePackage;
import com.ardab.subsapp.mobilepackage.rest.MobilePackageRestService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/v1/packages")
public class MobilePackageController {

    @Autowired
    MobilePackageRestService restService;

    @GetMapping
    public ResponseEntity<List<MobilePackage>> getAllPackages(){
        return restService.getAllPackages();
    }

    @GetMapping("/{id}")
    public ResponseEntity<MobilePackage> getPackageById(@PathVariable int id) throws NotFoundException {
        return restService.getPackageById(id);
    }

    @PostMapping("/create")
    public ResponseEntity<MobilePackage> createNewPackage(@RequestBody MobilePackage mobilePackage, HttpServletRequest request){
        return restService.createNewPackage(mobilePackage,request);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MobilePackage> updatePackage(@PathVariable int packageId, @RequestBody MobilePackage mobilePackage) throws NotFoundException {
        return restService.updatePackage(packageId,mobilePackage);
    }

    @DeleteMapping
    public void deletePackage(@PathVariable int packageId) throws NotFoundException {
        restService.deletePackage(packageId);
    }
}
