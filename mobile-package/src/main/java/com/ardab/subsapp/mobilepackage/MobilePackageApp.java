package com.ardab.subsapp.mobilepackage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilePackageApp {

    public static void main(String[] args) {
        SpringApplication.run(MobilePackageApp.class, args);
    }
}
