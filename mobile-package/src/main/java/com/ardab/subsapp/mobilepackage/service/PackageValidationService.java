package com.ardab.subsapp.mobilepackage.service;

import com.ardab.gprc.mobilepackage.MobilePackageErrorCode;
import com.ardab.subsapp.mobilepackage.exception.PackageException;
import com.ardab.subsapp.mobilepackage.repository.MobilePackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PackageValidationService {

    @Autowired
    MobilePackageRepository mobilePackageRepository;

    public void validatePackageCode(Integer packageId){
        checkPackageExist(packageId);
        checkPackageActive(packageId);
    }

    public void checkPackageExist(Integer packageId){
        if(!mobilePackageRepository.findById(packageId).isPresent()){
            throw new PackageException(MobilePackageErrorCode.INVALID_PACKAGE_CODE_VALUE);
        }
    }

    public void checkPackageActive(Integer packageId){
        if(mobilePackageRepository.findById(packageId).get().getPackageStatus()!=0){
            throw new PackageException(MobilePackageErrorCode.NOT_ACTIVE_PACKAGE);
        }
    }
}
