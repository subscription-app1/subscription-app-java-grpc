package com.ardab.subsapp.mobilepackage.exception;

import com.ardab.gprc.mobilepackage.MobilePackageErrorCode;
import lombok.Getter;

@Getter
public class PackageException extends RuntimeException{

    private MobilePackageErrorCode mobilePackageErrorCode;

    public PackageException(MobilePackageErrorCode errorCode){
        super(errorCode.name());
        this.mobilePackageErrorCode = errorCode;
    }
}
