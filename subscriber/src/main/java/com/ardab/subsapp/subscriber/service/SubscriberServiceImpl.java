package com.ardab.subsapp.subscriber.service;

import com.ardab.gprc.subscription.*;
import com.ardab.subsapp.subscriber.exception.SubscriberMissingFieldException;
import com.ardab.subsapp.subscriber.exception.SubscriberNotFoundException;
import com.ardab.subsapp.subscriber.model.Subscriber;
import com.ardab.subsapp.subscriber.model.SubscriberCreateRequest;
import com.ardab.subsapp.subscriber.model.SubscriptionCreateResponse;
import com.ardab.subsapp.subscriber.repository.SubscriberRepository;
import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.rpc.Status;
import io.grpc.protobuf.StatusProto;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
public class SubscriberServiceImpl implements SubscriberService{
    @GrpcClient("subscription")
    private SubscriptionServiceGrpc.SubscriptionServiceBlockingStub subscriptionStub;

    @Autowired
    SubscriberRepository subscriberRepository;


    @Override
    public ResponseEntity<List<Subscriber>> getAllSubscriber() {
        List<Subscriber> allSubscriber = subscriberRepository.findAll();
        return new ResponseEntity<List<Subscriber>>(allSubscriber,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Subscriber> getSubscriber(Long id) {
        Subscriber subscriber = getExistingSubscriber(id);
        return new ResponseEntity<Subscriber>(subscriber, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Subscriber> createNewSubscriber(SubscriberCreateRequest subscriber, HttpServletRequest request) {
        if(subscriber.getSubscriberMsisdn() != null && subscriber.getSubscriberName() != null && subscriber.getSubscriberSurname() != null){
            Subscriber newSubscriber = new Subscriber(subscriber.getSubscriberMsisdn(),subscriber.getSubscriberName(),subscriber.getSubscriberSurname(),subscriber.getSubscriberBalance());
            Subscriber createdSubscriber = subscriberRepository.saveAndFlush(newSubscriber);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Location", subscriberUrlHelper(createdSubscriber,request));
            return new ResponseEntity<Subscriber>(newSubscriber, responseHeaders, HttpStatus.CREATED);
        }
        else{
            throw new SubscriberMissingFieldException();
        }
    }

    @Override
    public ResponseEntity<Subscriber> updateSubscriber(Long id, SubscriberCreateRequest subscriber) {
        Subscriber existingSubscriber = getExistingSubscriber(id);
        if(subscriber.getSubscriberMsisdn() != null && subscriber.getSubscriberName() != null && subscriber.getSubscriberSurname() != null){
            BeanUtils.copyProperties(subscriber,existingSubscriber);
            existingSubscriber.setId(id);
            Subscriber updatedSubscriber = subscriberRepository.saveAndFlush(existingSubscriber);
            return new ResponseEntity<Subscriber>(updatedSubscriber,HttpStatus.OK);
        }
        else{
            throw new SubscriberMissingFieldException();
        }
    }

    @Override
    public ResponseEntity<Subscriber> deleteSubscriber(Long id) {
        Subscriber subscriber = getExistingSubscriber(id);
        subscriberRepository.delete(subscriber);
        return new ResponseEntity<Subscriber>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<SubscriptionCreateResponse> createSubscription(Long id, int packageId){
        Subscriber subscriber = getExistingSubscriber(id);
        SubscriptionRequest subscriptionRequest = SubscriptionRequest.newBuilder()
                .setSubscriberMsisdn(subscriber.getSubscriberMsisdn())
                .setSubscriptionPackage(packageId)
                .setSubscriberBalance(subscriber.getSubscriberBalance())
                .build();
        try {
            SubscriptionResponse subscriptionResponse = subscriptionStub.createNewSubscription(subscriptionRequest);
            SubscriptionCreateResponse response = new SubscriptionCreateResponse(
                    id,
                    subscriber.getSubscriberMsisdn(),
                    "Subscription Created",
                    subscriptionResponse.getCollectedPrice(),
                    subscriptionResponse.getSubscriptionStatus());
            subscriber.setSubscriptionPackage(packageId);
            subscriber.setSubscriberBalance(subscriber.getSubscriberBalance()- response.getCollectedPrice());
            subscriberRepository.saveAndFlush(subscriber);
            return new ResponseEntity<SubscriptionCreateResponse>(response,HttpStatus.CREATED);
        }catch (Exception e){
            Status status = StatusProto.fromThrowable(e);
            for(Any any : status.getDetailsList()){
                if(!any.is(SubscriptionExceptionResponse.class)){
                    continue;
                }
                try {
                    SubscriptionExceptionResponse exceptionResponse = any.unpack(SubscriptionExceptionResponse.class);
                    System.out.println("Error Code: "+exceptionResponse.getErrorCode());
                    SubscriptionCreateResponse response = new SubscriptionCreateResponse(
                            id,
                            subscriber.getSubscriberMsisdn(),
                            exceptionResponse.getErrorCode().toString(),
                            0.00,
                            -1);
                    return new ResponseEntity<SubscriptionCreateResponse>(response,HttpStatus.UNPROCESSABLE_ENTITY);
                } catch (InvalidProtocolBufferException exception){
                    exception.printStackTrace();
                }
            }
        }
        return null;
    }

    private String subscriberUrlHelper(Subscriber subscriber, HttpServletRequest request){
        StringBuilder resourcePath = new StringBuilder();
        resourcePath.append(request.getRequestURL());
        resourcePath.append("/");
        resourcePath.append(subscriber.getId());
        return resourcePath.toString();
    }

    private Subscriber getExistingSubscriber(Long id){
        Optional<Subscriber> existingSubscriber = subscriberRepository.findById(id);
        if(existingSubscriber.isPresent()){
            return existingSubscriber.get();
        }
        else{
            throw new SubscriberNotFoundException();
        }
    }
}
