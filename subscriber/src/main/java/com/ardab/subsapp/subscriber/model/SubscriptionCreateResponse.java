package com.ardab.subsapp.subscriber.model;

import lombok.Data;

@Data
public class SubscriptionCreateResponse {

    private Long id;
    private String msisdn;
    private String response;
    private double collectedPrice;
    private int subscriptionStatus;

    public SubscriptionCreateResponse(Long id, String msisdn, String response, double collectedPrice, int subscriptionStatus) {
        this.id = id;
        this.msisdn = msisdn;
        this.response = response;
        this.collectedPrice = collectedPrice;
        this.subscriptionStatus = subscriptionStatus;
    }
}
