package com.ardab.subsapp.subscriber.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Msisdn already exist!")
public class MsisdnExistException extends RuntimeException{
}
