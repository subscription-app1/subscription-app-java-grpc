package com.ardab.subsapp.subscriber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Must provide Msisdn,Name and Surname")
public class SubscriberMissingFieldException extends RuntimeException{
}
